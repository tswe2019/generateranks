import operator
import collections

def getMarksList(fhand):
    marklist = dict()
    for line in fhand:
        stu_data = line.strip().split()
        marks = int(stu_data[1]);
        rollno = stu_data[0];
        if marks not in marklist:
            marklist[marks] = [rollno]
        else:
            marklist[marks].append(rollno)
    return marklist

def sortMarkList(marklist):
    sorted_x = sorted(marklist.items(), key=operator.itemgetter(0), reverse=True)
    sortedDict = collections.OrderedDict(sorted_x)  
    return sortedDict;


def main():
    fhand = open('marklist.txt')    
    marklist = getMarksList(fhand);    
    sortedMarklist = sortMarkList(marklist)
    rank = 1;
    print("Rank\tRollno\tMark");
    for mark in sortedMarklist:
        print(str(rank), end=' ')
        for rollno in sortedMarklist[mark]:
            print("\t", rollno, "\t", mark, "\r")
            rank += 1;
   

if __name__=="__main__":
    main()
 

